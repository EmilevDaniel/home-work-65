import React from 'react';
import './Quote.css'

const Quote = ({title, content}) => {

    return (
        <div className='Post'>
            <h1>{title}</h1>
            <p>{content}</p>
        </div>
    );
};

export default Quote;