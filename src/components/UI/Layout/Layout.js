import React from 'react';
import './Layout.css'
import Toolbar from "../../Navigation/Toolbar/Toolbar";

const Layout = ({children}) => {
    return (
        <div>
            <Toolbar/>
            <main className='Layout-Content'>
                {children}
            </main>
        </div>
    );
};

export default Layout;