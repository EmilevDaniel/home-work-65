import axios from "axios";

const axiosApi = axios.create({
    baseURL: "https://emilev-app-for-js-11-default-rtdb.firebaseio.com/",
});

export default axiosApi;