import React, {useEffect, useState} from 'react';
import Spinner from "../../components/UI/Spinner/Spinner";
import axiosApi from "../../axiosApi";
import Quote from "../../components/Quote/Quote";

const MainPage = ({match}) => {

    const [quote, setQuote] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get(`/pages/${match.params.category}.json`);
            const fetchedQuotes = Object.keys(response.data).map(id => {
                return {...quote, id};
            });
            setQuote(fetchedQuotes);
        };
        fetchData().finally(() => setLoading(false))
    }, [match.params.category]);

    let ordersComponents = quote.map(quote => (
        <Quote
            key={quote.id}
            title={quote.ingredients}
            content={quote.customer}
        />
    ));

    if (loading) {
        ordersComponents = <Spinner/>
    }

    return ordersComponents
};

export default MainPage;