import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Layout from "./components/UI/Layout/Layout";
import MainPage from "./pages/MainPage/MainPage";


function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path='/' exact component={null}/>
                    <Route path='/pages/:category' exact component={MainPage}/>
                    <Route render={() => <h1>Not found</h1>}/>
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
